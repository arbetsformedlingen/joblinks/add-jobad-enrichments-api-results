import ndjson
import os
import sys
import itertools

def read_ads_from_std_in():
    ads = ndjson.load(sys.stdin)
    return ads

def read_ads_from_file(rel_filepath):
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    ads_path = currentdir + rel_filepath
    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads

def print_ads(ads):
    output_json = ndjson.dumps(ads, ensure_ascii=False)
    print(output_json, file=sys.stdout)

def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def get_null_safe_value(element, key, replacement_val):
    val = element.get(key, replacement_val)
    if val is None:
        val = replacement_val
    return val

def is_valid_ad(ad):
    is_valid = False
    if 'id' in ad and ad['id'] and 'originalJobPosting' in ad:
        doc_headline = get_null_safe_value(ad['originalJobPosting'], 'title', '')
        doc_description = get_null_safe_value(ad['originalJobPosting'], 'description', '')
        is_valid = doc_headline or doc_description
    return is_valid

def validate_ads(ads):
    valid_ads = []
    non_valid_ads = []
    for ad in ads:
        if is_valid_ad(ad):
            valid_ads.append(ad)
        else:
            non_valid_ads.append(ad)
    return valid_ads, non_valid_ads

def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])

def str_to_bool(s):
    if s and (s.lower() == 'true' or s.lower() == 'yes'):
        return True
    else:
        return False

def print_debug(msg):
    # Assumes print_debug_messages is defined in the caller module.
    if 'print_debug_messages' in globals() and print_debug_messages:
        print(msg)
