import concurrent.futures
import copy
import itertools
import os
import sys
import ndjson
import json
import requests
from nested_lookup import nested_delete
LANGCODE_ENG = 'en'
ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
from utils import read_ads_from_std_in, read_ads_from_file, print_ads, print_error, get_null_safe_value, is_valid_ad, validate_ads, grouper, str_to_bool, print_debug


def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    print_debug('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json', 'api-key': jae_api_key}

    try:
        r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
        r.raise_for_status()
    except Exception as e:
        print_error(e)
        raise
    else:  # no error
        return r.json()


def execute_calls(batch_indatas, jae_api_endpoint_url, parallelism):
    global counter
    enriched_output = {}
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(get_enrich_result, batch_indata, jae_api_endpoint_url,
                                                   jae_api_timeout): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for resultrow in enriched_result:
                    enriched_output[resultrow[ENRICHER_PARAM_DOC_ID]] = resultrow
            except Exception as e:
                print_error(e)
                raise

    return enriched_output


def enrich(ad_batches, batch_indata_config, endpoint_url):
    batch_indatas = []
    for i, ad_batch in enumerate(ad_batches):
        ad_batch_indatas = [ad_indata for ad_indata in ad_batch]
        batch_indata = copy.deepcopy(batch_indata_config)
        batch_indata['documents_input'] = ad_batch_indatas
        batch_indatas.append(batch_indata)
    print_debug('len(batch_indatas): %s' % len(batch_indatas))
    enrich_results_data = execute_calls(batch_indatas, endpoint_url, parallelism)
    # print_debug('enrich_results_data: %s' % enrich_results_data)

    return enrich_results_data


def read_env_variables():
    global local_filepath
    local_filepath = os.getenv('LOCAL_FILEPATH', '')

    global print_debug_messages
    print_debug_messages = str_to_bool(os.getenv('PRINT_DEBUG', 'False'))

    global jae_api_timeout
    jae_api_timeout = int(os.getenv('JAE_API_TIMEOUT_SECONDS', '60'))

    global jae_api_url
    jae_api_url = os.getenv('JAE_API_URL', 'https://jobad-enrichments-api.jobtechdev.se')

    print_debug('jae_api_url: %s' % jae_api_url)

    global jae_api_key
    jae_api_key = os.getenv('JAE_API_KEY', '')

    if not jae_api_key:
        print_error('Env variable JAE_API_KEY has no value and must be set!')
        sys.exit(1)

    print_debug(jae_api_key)




def read_taxonomy_municipalities_data_from_file():
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    taxonomyvalues_filepath = currentdir + '../resources/taxonomy_v1_municipalities_regions_countries.json'
    with open(taxonomyvalues_filepath, encoding="utf8") as tax_file:
        municipalities_data = []
        taxonomy_response = json.load(tax_file)
        data = taxonomy_response['data']
        if data:
            concepts = data['concepts']
            if concepts:
                for concept in concepts:
                    municipalities_data.append(concept)

        return municipalities_data


def find_scraped_joblocation(ad):
    scraped_joblocation = None
    if 'originalJobPosting' in ad and 'jobLocation' in ad['originalJobPosting']:
        joblocation = ad['originalJobPosting']['jobLocation']
        # Info: Some of the scrapers saves the city in attribute 'address' and some of them in the attribute 'name'
        if 'address' in joblocation and joblocation['address']:
            # Info: address can either be a string with the city or a dict according to https://schema.org/PostalAddress
            address = joblocation['address']
            if isinstance(address, str):
                scraped_joblocation = joblocation['address'].lower()
            elif isinstance(address, dict):
                if 'addressLocality' in address:
                    scraped_joblocation = address['addressLocality'].lower()
        elif 'name' in joblocation and joblocation['name']:
            scraped_joblocation = joblocation['name'].lower()

    return scraped_joblocation


def get_doc_headline_input(ad):
    sep = ' | '

    headline_values = []

    scraped_joblocation = find_scraped_joblocation(ad)
    if scraped_joblocation:
        headline_values.append(scraped_joblocation)

        if scraped_joblocation in municipality_taxonomy_mappings:
            tax_item = municipality_taxonomy_mappings[scraped_joblocation]
            region = tax_item['region_preferred_label'].lower()
            headline_values.append(region)

    keywords = get_null_safe_value(ad['originalJobPosting'], 'keywords', [])

    if len(keywords) > 0:
        headline_keywords = sep.join(keywords)
        headline_values.append(headline_keywords)

    doc_headline = get_null_safe_value(ad['originalJobPosting'], 'title', '')
    if doc_headline:
        headline_values.append(doc_headline)

    doc_headline_input = sep.join(headline_values)

    print_debug("doc_headline_input: %s" % doc_headline_input)

    return doc_headline_input


def read_municipality_taxonomy_mappings():
    global municipality_taxonomy_mappings
    municipalities_taxonomy_data = read_taxonomy_municipalities_data_from_file()
    municipality_taxonomy_mappings = {}
    for municipality_entry in municipalities_taxonomy_data:
        municipality_taxonomy_mappings[municipality_entry['preferred_label'].lower()] = {
            'mun_id': municipality_entry['id'],
            'mun_preferred_label': municipality_entry['preferred_label'],
            'region_id': municipality_entry['broader'][0]['id'],
            'region_preferred_label': municipality_entry['broader'][0]['preferred_label'],
            'country_id': municipality_entry['broader'][0]['broader'][0]['id'],
            'country_preferred_label': municipality_entry['broader'][0]['broader'][0]['preferred_label']
        }
    return municipality_taxonomy_mappings


def add_enriched_result(ads_to_update, enriched_results_data):
    enriched_attribute_name = 'enriched_result'

    for ad_to_update in ads_to_update:
        if not 'text_enrichments_results' in ad_to_update:
            ad_to_update['text_enrichments_results'] = {}
        doc_id = str(ad_to_update.get('id', ''))
        if doc_id in enriched_results_data:
            enriched_output = enriched_results_data[doc_id]
            enriched_output.pop(ENRICHER_PARAM_DOC_ID, None)
            enriched_output.pop(ENRICHER_PARAM_DOC_HEADLINE, None)
        else:
            # Set non valid ads to empty enriched values.
            enriched_output = get_empty_output_value()

        ad_to_update['text_enrichments_results'][enriched_attribute_name] = enriched_output

def add_enriched_binary_result(ads_to_update, enriched_bin_results_data):
    enriched_attribute_name = 'enrichedbinary_result'

    for ad_to_update in ads_to_update:
        ad_detected_language = None
        if 'detected_language' in ad_to_update:
            ad_detected_language = ad_to_update['detected_language']

        if not 'text_enrichments_results' in ad_to_update:
            ad_to_update['text_enrichments_results'] = {}
        doc_id = str(ad_to_update.get('id', ''))

        if ad_detected_language and LANGCODE_ENG == ad_detected_language and 'enriched_result' in ad_to_update['text_enrichments_results']:
            enriched_output = copy_distinct_values_from_enriched_result(ad_to_update)
            enriched_output = remove_english_stopwords(enriched_output)
        elif doc_id in enriched_bin_results_data:
            enriched_output = enriched_bin_results_data[doc_id]
            enriched_output.pop(ENRICHER_PARAM_DOC_ID, None)
            enriched_output.pop(ENRICHER_PARAM_DOC_HEADLINE, None)
        else:
            # Set non valid ads to empty enriched values.
            enriched_output = get_empty_output_value()

        ad_to_update['text_enrichments_results'][enriched_attribute_name] = enriched_output


def copy_distinct_values_from_enriched_result(ad_to_update):
    enriched_output = copy.deepcopy(ad_to_update['text_enrichments_results']['enriched_result'])
    enriched_output = nested_delete(enriched_output, 'prediction')
    enriched_candidates = enriched_output['enriched_candidates']
    for key, value in enriched_candidates.items():
        candidates_for_type = enriched_candidates[key]
        unique_candidates_for_type = list({v['concept_label']: v for v in candidates_for_type}.values())
        enriched_candidates[key] = unique_candidates_for_type
    return enriched_output

def remove_english_stopwords(enriched_output):
    enriched_candidates = enriched_output['enriched_candidates']
    for key, value in enriched_candidates.items():
        candidates_for_type = enriched_candidates[key]
        # unique_candidates_for_type = list({v['concept_label']: v for v in candidates_for_type}.values())
        if 'occupations' == key:
            stopwords = ['ad', 'general', 'do', 'host', 'lots', 'major', 'medium', 'traffic', 'trainer']
            cleaned_candidates_for_type = filter_candidates(candidates_for_type, stopwords)
        elif 'competencies' == key:
            stopwords = ['basic', 'brand', 'can', 'client', 'country', 'far',
                         'fastest', 'its', 'less', 'material', 'mission', 'most', 'nice', 'not',
                         'office', 'present', 'show', 'smart']
            cleaned_candidates_for_type = filter_candidates(candidates_for_type, stopwords)
        elif 'traits' == key:
            stopwords = ['driven']
            cleaned_candidates_for_type = filter_candidates(candidates_for_type, stopwords)
        elif 'geos' == key:
            stopwords = ['ask', 'long']
            cleaned_candidates_for_type = filter_candidates(candidates_for_type, stopwords)
        else:
            cleaned_candidates_for_type = candidates_for_type

        enriched_candidates[key] = cleaned_candidates_for_type
    return enriched_output


def filter_candidates(candidates_for_type, stopwords):
    return [cand for cand in candidates_for_type if cand['term'].lower() not in stopwords]


def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }


if __name__ == '__main__':

    read_env_variables()

    parallelism = int(os.getenv('ENRICH_CALLS_PARALLELISM', '2'))

    if parallelism <= 0:
        parallelism = 1

    print_debug('parallelism: %s' % parallelism)

    if local_filepath:
        print_debug('Reading ads from: %s' % local_filepath)
        ads = read_ads_from_file(local_filepath)
    else:
        ads = read_ads_from_std_in()

    municipality_taxonomy_mappings = read_municipality_taxonomy_mappings()

    nr_of_items_per_batch = 100

    valid_ads, non_valid_ads = validate_ads(ads)

    print_debug('len(valid_ads): %s, len(non_valid_ads): %s' % (len(valid_ads), len(non_valid_ads)))

    if len(non_valid_ads) > 0:
        print_error('The following ads are not valid and cant be enriched: ' % [ad['id'] for ad in non_valid_ads])

    # Prepare input
    ads_input_data = []
    for ad in valid_ads:
        doc_id = str(ad.get('id', ''))
        doc_headline = get_doc_headline_input(ad)
        doc_text = get_null_safe_value(ad['originalJobPosting'], 'description', '')

        input_doc_params = {
            ENRICHER_PARAM_DOC_ID: doc_id,
            ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
            ENRICHER_PARAM_DOC_TEXT: doc_text
        }

        ads_input_data.append(input_doc_params)

    ad_batches = grouper(nr_of_items_per_batch, ads_input_data)

    batch_indata_config = {
        "include_terms_info": True,
        "include_sentences": False,
        # "sort_by_prediction_score": "DESC"
        "sort_by_prediction_score": "NOT_SORTED"
    }

    enrich_results = enrich(ad_batches, batch_indata_config, jae_api_url + '/enrichtextdocuments')
    add_enriched_result(ads, enrich_results)

    ad_batches_binary = grouper(nr_of_items_per_batch, ads_input_data)
    batch_indata_binary_config = {
        "include_terms_info": True,
        "include_sentences": False
    }

    enrich_binary_results = enrich(ad_batches_binary, batch_indata_binary_config,
                                   jae_api_url + '/enrichtextdocumentsbinary')

    add_enriched_binary_result(ads, enrich_binary_results)

    # Info: Just to make development easier. Filter english ads only.
    # ads = [ad for ad in ads if ad['detected_language'] == 'en']

    print_ads(ads)
