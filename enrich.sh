#!/usr/bin/env bash

apikey=
while [ "$#" -gt 0 ]; do
    case "$1" in
    --apikey) apikey="$2"; shift ;;
    *)       echo "**** unknown opt: $1" >&2; exit 1;;
    esac
    shift
done

. ./docker.env || exit 1

export JAE_API_URL
export JAE_API_TIMEOUT_SECONDS
export ENRICH_CALLS_PARALLELISM
export PRINT_DEBUG

# Run the application
env JAE_API_KEY="${apikey}" python3 src/main.py
