FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app

COPY enrich.sh .
COPY requirements.txt .
COPY run.sh .
COPY docker.env .
COPY resources/ resources/
COPY src/ src/

RUN apt-get -y update &&\
    apt-get -y install python3 python3-pip &&\
    python3 -m pip install -r requirements.txt


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN apt-get -y update && apt-get -y install jq

# Predictions are truncated to 5 decimals, to
# avoid failing tests because of too small differences
RUN env JAE_API_KEY=dummyvalue python3 src/main.py               < /testdata/input.10 \
      | jq '.text_enrichments_results.enriched_result.enriched_candidates.occupations[].prediction=0.1' \
      | jq '.text_enrichments_results.enriched_result.enriched_candidates.competencies[].prediction=0.1' \
                                                            > /tmp/output.10
RUN perl -pe 's|("prediction": .{7})(?:\d+)(.+)|$1$2|g'     < /testdata/expected_output.10 \
      | jq '.text_enrichments_results.enriched_result.enriched_candidates.occupations[].prediction=0.1' \
      | jq '.text_enrichments_results.enriched_result.enriched_candidates.competencies[].prediction=0.1' \
                                                            > /tmp/expected_output.10
RUN diff /tmp/expected_output.10 /tmp/output.10 \
      && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

ENTRYPOINT [ "sh", "enrich.sh" ]
