# add-jobad-enrichments-api-results
Pipeline step that adds text enrichment with JobAd Enrichments API from the ad headline and ad description.
Before enrichment call, the headline contains data from the scraped jobLocation, if present.  
  
## Prerequisites  
Docker and a bash shell  
  
## Needed input data for running  
The step needs a list of ads with the attributes/structure:    
{"originalJobPosting": {"id": "52fcd194da9b01ccc042fc98dc1ad0e7", "title": "Rubrik för annons 1 här", "description": "Annonsbeskrivningen för annons 1 här" }}  
{"originalJobPosting": {"id": "37d918ffd5bdffb0723440a41014b844", "title": "Rubrik för annons 2 här",  "description": "Annonsbeskrivningen för annons 2 här" }}  

## Building Docker image  
./run.sh --build  
  
## Running Docker with ads on stdin  
cat /tmp/ads_input  |  ./run.sh    >/tmp/ads_added_jobad_enrichments_api_output    
cat resources/add_id_2  |  ./run.sh    > resources/ads_added_jobad_enrichments_2_out    
  
## Clean Docker image  
./run.sh --clean  
  
## Mac, remove previous docker-builds + cointainers  
docker system prune  
  
## Start shell in Docker image  
docker run -it joblinks/add-jobad-enrichments-api-results sh  
  
## Install for development   
pip install -r requirements.txt  
  
## Running with python and file as input (during development)  
If using ads file instead of stdin; put a pipeline file with ads in resources dir, set local env variable
export LOCAL_FILEPATH=resources/input_filename
..and run with:    
python3 src/main.py  
